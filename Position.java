/**
 * Une position sur la grille, en coordonnées écran. Cette classe est immuable
 * @author Boris DRYKONINGEN
*/
public class Position {
    /**
     * La position sur l'axe des abscisses. Plus x est grand, plus cette
     * position est à droite
    */
    private int x;


    /**
     * La position sur l'axe des ordonnées. Plus y est grand, plus cette
     * position est en bas
    */
    private int y;


    /**
     * La largeur de la grille
    */
    private int largeur;


    /**
     * La hauteur de la grille
    */
    private int hauteur;


    /**
     * Construit une position de coordonnées arbitraires
     * @param largeur la largeur de la Grille
     * @param hauteur la hauteur de la Grille
     * @param x la position sur l'axe des abscisses (doit être strictement
     * positif et strictement inférieur à la largeur de la grille)
     * @param y la position sur l'axe des ordonnées (doit être strictement
     * positif et strictement inférieur à la hauteur de la grille)
     * @throws IllegalArgumentException si x ou y sont invalides dans la grille
     * donnée
    */
    private Position(int largeur, int hauteur, int x, int y) {
        if (x < 0 || y < 0 || x >= largeur || y >= hauteur) {
            throw new IllegalArgumentException(
                "Tentative de construire une position invalide"
            );
        }

        this.largeur = largeur;
        this.hauteur = hauteur;
        this.x = x;
        this.y = y;
    }


    /**
     * Construit une position de coordonnées arbitraires
     * @param grille la grille à laquelle cette position réfère
     * @param x la position sur l'axe des abscisses (doit être strictement
     * positif et strictement inférieur à la largeur de la grille)
     * @param y la position sur l'axe des ordonnées (doit être strictement
     * positif et strictement inférieur à la hauteur de la grille)
     * @throws IllegalArgumentException si x ou y sont invalides dans la grille
     * donnée
    */
    public Position(GrilleGraphique grille, int x, int y) {
        this(grille.getLargeur(), grille.getHauteur(), x, y);
    }


    /**
     * Obtient l'abscisse de cette instance
     * @return l'abscisse de cette instance
    */
    public int getX() {
        return x;
    }


    /**
     * Obtient l'ordonnée de cette instance
     * @return l'ordonnée de cette instance
    */
    public int getY() {
        return y;
    }


    /**
     * Construit une nouvelle position par translation de la position courante.
     * Réalise un rebond si un bord de la grille est atteint
     * @param translation la translation à appliquer
     * @return La position construite par translation
    */
    public Position deplace(Direction translation) {
        int nvX = x + translation.getX();
        int nvY = y + translation.getY();

        if (nvX < 0 || nvX >= largeur) {
            return deplace(new Direction(-translation.getX(), translation.getY()));
        } else if (nvY < 0 || nvY >= hauteur) {
            return deplace(new Direction(translation.getX(), -translation.getY()));
        } else {
            return new Position(
                largeur,
                hauteur,
                nvX,
                nvY
            );
        }
    }


    /**
     * Compare deux positions
     * @param rhs la position à laquelle comparer la position actuelle
     * @return true si les deux positions sont définies comme égales, false
     * sinon
    */
    public boolean equals(Position rhs) {
        return x == rhs.x && y == rhs.y;
    }
}

