import java.util.ArrayList;


/**
 * Une Grille sur laquelle se déplacent les personnages
 * @author Boris DRYKONINGEN
*/
public class GrilleGraphique {
    /**
     * La largeur de la grille
    */
    private int largeur;


    /**
     * La hauteur de la grille
    */
    private int hauteur;


    /**
     * Les cellules de la grille à dessiner. A hauteur lignes et largeur
     * colonnes. Aussi, il est possible de dessiner n'importe quel objet sur la
     * Grille en écrivant dans cellules[y][x]
    */
    private char[][] cellules;


    /**
     * Les messages à afficher à l'utilisateur. Affichés après l'affichage de la
     * grille
    */
    private ArrayList<String> messages;


    /**
     * Le caractère écrit dans les cellules vides
    */
    private static final char CELLULE_VIDE = '.';


    /**
     * Construit une nouvelle Grille
     * @param largeur la largeur de la Grille
     * @param hauteur la hauteur de la Grille
    */
    public GrilleGraphique(int largeur, int hauteur) {
        this.largeur = largeur;
        this.hauteur = hauteur;
        cellules = new char[hauteur][largeur];
        raffraichitEcran();
        messages = new ArrayList<String>();
    }


    /**
     * Remplace toutes les cellules par des cellules vides
    */
    private void raffraichitEcran() {
        for (int i = 0; i != cellules.length; ++i) {
            for (int j = 0; j != cellules[0].length; ++j) {
                cellules[i][j] = CELLULE_VIDE;
            }
        }
    }


    /**
     * Donne accès à la largeur de la grille
     * @return la largeur de la grille
    */
    public int getLargeur() {
        return largeur;
    }


    /**
     * Donne accès à la hauteur de la grille
     * @return la hauteur de la grille
    */
    public int getHauteur() {
        return hauteur;
    }


    /**
     * Dessine le caractère donné dans la Grille. Ne l'affiche pas, écrit
     * seulement dans la mémoire que ce caractère doit être affiché à la
     * position donnée dans la Grille
     * @param position sa position dans la Grille
     * @param apparence le caractère à afficher
     * @throws IllegalArgumentException si apparence n'est pas utilisable dans
     * ce contexte. Cela peut traduire l'usage d'un caractère réservé ou un
     * caractère non-graphique
     * @see affiche
    */
    public void dessine(Position position, char apparence) {
        if ((!Character.isLetterOrDigit(apparence) && !Character.isIdeographic(apparence))
            || (apparence == '+' || apparence == '-' || apparence == '|' || apparence == CELLULE_VIDE)
        ) {
            throw new IllegalArgumentException("Caractère invalide utilisé pour l'affichage");
        }

        cellules[position.getY()][position.getX()] = apparence;
    }


    /**
     * Envoie un message à l'utilisateur. Ce message sera affiché après la
     * grille et son contenu
     * @param msg le message à transmettre. Doit tenir sur une ligne
     * @throws IllegalArgumentException si le message contient un retour à la ligne
    */
    void envoieMessage(String msg) {
        for (int i = 0; i != msg.length(); ++i) {
            if (msg.charAt(i) == '\n') {
                throw new IllegalArgumentException("Message sur plusieurs lignes !");
            }

        }
        messages.add(msg);
    }


    /**
     * Affiche un séparateur de lignes. Utilisé pour tracer un trait entre deux
     * lignes dans la méthode affiche
    */
    private void afficheSeparateur() {
        Ecran.afficher('+');
        for (int i = 0; i != cellules[0].length; ++i) {
            Ecran.afficher("---+");
        }
        Ecran.afficherln();
    }


    /**
     * Dessine la Grille à l'écran. Toutes les modifications apportées à l'état
     * de la Grille seront répercutées à l'affichage puis oubliées
     * @see dessine
    */
    public void affiche() {
        for (int i = 0; i != cellules.length; ++i) {
            // Séparateur de lignes
            afficheSeparateur();
            // Ligne
            Ecran.afficher('|');
            for (int j = 0; j != cellules[0].length; ++j) {
                Ecran.afficher(' ', cellules[i][j], " |");
            }
            Ecran.afficherln();
        }

        // Séparateur supplémentaire à la fin de la Grille
        afficheSeparateur();

        for (String i: messages) {
            Ecran.afficherln(i);
        }

        messages.clear();
        raffraichitEcran();
    }
}

