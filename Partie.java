import java.util.ArrayList;


/**
 * Le placement des joueurs et équipes sur la grille
 * @author Boris DRYKONINGEN
*/
public class Partie {
    /**
     * Les différentes équipes, elles-mêmes composées de différents joueurs
    */
    ArrayList<ArrayList<Player>> equipes;


    /**
     * Un cache pour déterminer en O(1) le joueur qui se trouve sur une case
     * donnée de la grille
    */
    private Player[][] positionsJoueurs;


    /**
     * La sortie chargée d'afficher la grille et des messages à destination de
     * l'utilisateur
    */
    private GrilleGraphique grilleGraphique;


    /**
     * Vrai si la partie a été interrompue manuellement, faux sinon
    */
    private boolean interrompu;


    /**
     * Construit une nouvelle partie
     * @param largeur la largeur de la grille à créer
     * @param hauteur la hauteur de la grille à créer
     * @param nbEquipes le nombre d'équipes à instancier
    */
    public Partie(int largeur, int hauteur, int nbEquipes) {
        equipes = new ArrayList<ArrayList<Player>>();
        for (int i = 0; i != nbEquipes; ++i) {
            equipes.add(new ArrayList<Player>());
        }
        positionsJoueurs = new Player[hauteur][largeur];
        grilleGraphique = new GrilleGraphique(largeur, hauteur);
        interrompu = false;
    }


    /**
     * Lance la partie. L'exécution ne s'arrête que quand la partie est terminée
    */
    public void jouer() {
        for (int i = 0; i != equipes.size(); ++i) {
            for (int j = 0; j != equipes.get(i).size(); ++j) {
                Player joueur = equipes.get(i).get(j);
                grilleGraphique.dessine(joueur.getPosition(), joueur.getAppearence());
            }
        }
        grilleGraphique.envoieMessage("État initial de la grille");
        grilleGraphique.envoieMessage("Appuyez sur Entrée pour jouer, ou tapez q ou exit pour quitter");
        grilleGraphique.affiche();

        while (!estTermine()) {
            alternanceEquipes();
            interactionsUtilisateur();
        }

        int equipeGagnante = -1;
        for (int i = 0; i != equipes.size(); ++i) {
            if (!equipes.get(i).isEmpty()) {
                equipeGagnante = i;
                break;
            }
        }
        Ecran.afficherln("Le vainqueur est l'équipe ", equipeGagnante + 1);
    }


    /**
     * Indique si la partie est terminée. Soit elle est terminée car une
     * condition de fin est remplie, comme la mort de toutes les équipes sauf
     * une, soit la partie est terminée car interrompue par l'utilisateur
     * @return vrai si la partie est terminée, false sinon
    */
    private boolean estTermine() {
        int nbEquipesRestantes = 0;
        for (int i = 0; i != equipes.size(); ++i) {
            if (!equipes.get(i).isEmpty()) {
                ++nbEquipesRestantes;
            }
        }

        return interrompu || nbEquipesRestantes <= 1;
    }


    /**
     * Indique les effectifs de l'équipe la plus grande
     * @return le nombre de joueurs dans l'équipe ayant le plus de joueurs
    */
    private int effectifsPlusGrandeEquipe() {
        int longueurMax = 0;
        for (int i = 0; i != equipes.size(); ++i) {
            longueurMax = Math.max(equipes.get(i).size(), longueurMax);
        }

        return longueurMax;
    }


    /**
     * Fait jouer les joueurs de chaque équipe selon le principe d'alternance
    */
    private void alternanceEquipes() {
        for (int i = 0; i != effectifsPlusGrandeEquipe(); ++i) {
            for (int j = 0; j != equipes.size(); ++j) {
                ArrayList<Player> equipeActuelle = equipes.get(j);
                if (equipeActuelle.size() > i) {
                    grilleGraphique.envoieMessage("Le " + (i + 1) + "° personnage de l'équipe " + (j + 1) + " joue !");
                    jouerJoueur(equipeActuelle.get(i));
                }
            }
        }
    }


    /**
     * Déplace le joueur souhaité sur la grille
     * @param joueur le joueur à déplacer
     * @param dst la destination du joueur
    */
    private void deplaceJoueur(Player joueur, Position dst) {
        Position src = joueur.getPosition();
        joueur.setPosition(dst);
        positionsJoueurs[src.getY()][src.getX()] = null;
        positionsJoueurs[dst.getY()][dst.getX()] = joueur;
    }


    /**
     * Génère un nombre aléatoire dans une intervalle donnée
     * @param bas la limite basse de l'intervalle (incluse)
     * @param haut la limite haute de l'intervalle (non-incluse)
     * @return le nombre généré
    */
    private int nombreAleatoire(int bas, int haut) {
        return ((int) (Math.random() * (haut - bas))) + bas;
    }


    /**
     * Fait jouer un joueur particulier
     * @param joueur le joueur à faire jouer
    */
    private void jouerJoueur(Player joueur) {
        Direction[] directionsTheoriques = {
            new Direction(-1, -1), new Direction(0, -1), new Direction(1, -1),
            new Direction(-1,  0),  /* Not available */  new Direction(1,  0),
            new Direction(-1,  1), new Direction(0,  1), new Direction(1,  1),
        };

        ArrayList<Position> positionsPossibles = new ArrayList<Position>();
        for (int i = 0; i != directionsTheoriques.length; ++i) {
            Position pos = joueur.getPosition().deplace(directionsTheoriques[i]);
            Player occupant = positionsJoueurs[pos.getY()][pos.getX()];
            if (occupant == null || occupant.getTeam() != joueur.getTeam()) {
                positionsPossibles.add(pos);
            }
        }

        if (positionsPossibles.isEmpty()) {
            grilleGraphique.envoieMessage(joueur + " ne peut rien faire");
            return;
        }

        Position nouvPos = positionsPossibles.get(nombreAleatoire(0, positionsPossibles.size()));
        if (positionsJoueurs[nouvPos.getY()][nouvPos.getX()] == null) {
            deplaceJoueur(joueur, nouvPos);
            grilleGraphique.envoieMessage("Le joueur " + joueur + " va sur une case vide");
        } else {
            Player cible = positionsJoueurs[nouvPos.getY()][nouvPos.getX()];
            joueur.attacked(grilleGraphique, cible);

            if (!cible.isAlive()) {
                grilleGraphique.envoieMessage("Le joueur " + joueur + " a vaincu son ennemi et prend sa place !");
                effaceJoueur(cible);
                deplaceJoueur(joueur, nouvPos);
            } else {
                // On ne peut pas se déplacer sur une case occupée, même par un
                // ennemi. Mais les cases libres maintenant sont un
                // sous-ensemble des cases libres à la première passe
                ArrayList<Position> positionsVides = new ArrayList<Position>();
                for (int i = 0; i != positionsPossibles.size(); ++i) {
                    Position essai = positionsPossibles.get(i);
                    if (positionsJoueurs[essai.getY()][essai.getX()] == null) {
                        positionsVides.add(positionsPossibles.get(i));
                    }
                }

                if (!positionsVides.isEmpty()) {
                    deplaceJoueur(joueur, positionsVides.get(nombreAleatoire(0, positionsVides.size())));
                }
            }
        }
    }


    /**
     * Efface toute trace d'un joueur
     * @param cadavre le joueur (mort) à supprimer
     * @throws IllegalStateException si le joueur est vivant
    */
    void effaceJoueur(Player cadavre) {
        if (cadavre.isAlive()) {
            throw new IllegalStateException("Tentative d'effacer un joueur en vie !");
        }

        equipes.get(cadavre.getTeam()).remove(cadavre);
    }


    /**
     * Ajoute un joueur à une équipe
     * @param joueur le joueur à ajouter
     * @throws IllegalStateException si le joueur est à une position déjà prise
    */
    public void ajouterJoueur(Player joueur) {
        equipes.get(joueur.getTeam()).add(joueur);
        Position pos = joueur.getPosition();
        if (positionsJoueurs[pos.getY()][pos.getX()] != null) {
            throw new IllegalStateException("Le joueur est à une position occupée");
        }
        positionsJoueurs[pos.getY()][pos.getX()] = joueur;
    }


    /**
     * Donne accès à l'afficheur (nécessaire pour créer des positions)
     * @return l'afficheur
    */
    public GrilleGraphique getGrilleGraphique() {
        return grilleGraphique;
    }


    /**
     * Gère les interractions avec l'utilisateur. Met à jour l'état de la partie
     * en fonction
    */
    private void interactionsUtilisateur() {
        for (int i = 0; i != equipes.size(); ++i) {
            for (int j = 0; j != equipes.get(i).size(); ++j) {
                Player joueur = equipes.get(i).get(j);
                grilleGraphique.dessine(joueur.getPosition(), joueur.getAppearence());
            }
        }
        grilleGraphique.affiche();

        Ecran.afficherln("Veuillez taper une commande ou appuyer sur Entrée pour passer au tour suivant");
        String cmd = Clavier.saisirString();
        if (cmd.equals("exit")) {
            interrompu = true;
        }
    }
}

