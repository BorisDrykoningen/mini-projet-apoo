// Auteur : Imane
public class Health{
  private int life,resurrection;
  public Health(int life,int resurrection){
    this.life = life;
    this.resurrection = resurrection;
  }
  int getLife(){
    return life;
  }
  int getResurrection(){
    return resurrection;
  }
  public void setLife(int life){
    this.life = life;
  }
  public void setResurrection(int resurrection){
    this.resurrection = resurrection;
  }

}
