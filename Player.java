// Auteur : Imane
// Ajout du support des équipes et messages + patch de bugs : Boris
public class Player{
  Position position;
  char appearence;
  String name;
  Defense defense ;
  Attack attack ;
  Health health;
  int team;
  public Position getPosition(){
    //prend pas de parametre et renvois la position du joueur//
    return position;
  }
  public void setPosition(Position position){
    //prens en para une position et elle la change//
    this.position=position;
  }
  public char getAppearence(){
    //prend pas de para et renvoie le caractere à afficher sur la case ou est le personnage//
    return appearence;
  }
  int entierAleatoire(int min,int max){
    return ((int) (Math.random() * (max - min + 1))) + min;
  }
  public int attacked(GrilleGraphique grilleGraphique, Player cible){
    int damage;
    if (attack.getSkill() >(cible.defense.getDodge() - entierAleatoire(1,6))){
      damage = ((attack.getStrength() + entierAleatoire(1,6))-cible.defense.getArmor());
    }
    else {
      grilleGraphique.envoieMessage("Le joueur " + this + " attaque " + cible + " mais ne le touche pas");
      return 0;
    }

    if (damage <= 0) {
        damage = 0;
    }

    int remainingLife = cible.health.getLife() - damage;
    if (remainingLife > 0) {
        grilleGraphique.envoieMessage("Le joueur " + this + " attaque " + cible + " et lui inflige " + damage + " dégâts !");
    } else {
        if (cible.health.getResurrection() > -remainingLife) {
            grilleGraphique.envoieMessage("Le joueur " + this + " tue " + cible + " qui ressucite !");
            int healingAmount = entierAleatoire(-remainingLife + 1, cible.health.getResurrection());
            cible.health.setResurrection(cible.health.getResurrection() - healingAmount);
            remainingLife += healingAmount;
        } else {
            grilleGraphique.envoieMessage("Le joueur " + this + " tue " + cible + " !");
            remainingLife = 0;
        }
    }
    cible.health.setLife(remainingLife);
    return damage;
  }
  public boolean isAlive(){
    if (health.getLife() > 0)
      return true;
    else
      return false;
  }
  public String toString(){
    return(name + " ["+appearence+":"+"(H:"+attack.getSkill()+",F:"+attack.getStrength()+"),(A:"+defense.getArmor()+",E:"+defense.getDodge()+"),(V:"+health.getLife()+",R:"+health.getResurrection()+")]");
  }


  public int getTeam() {
      return team;
  }


  public Player(String name, char appearence, int team, Position pos, Attack attack, Defense defense, Health health){
    this.name=name;
    this.appearence=appearence;
    this.team = team;
    position=pos;
    this.attack=attack;
    this.defense=defense;
    this.health=health;
  }
}

