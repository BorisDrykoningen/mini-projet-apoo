// Auteur : Imane
public class Defense{
  private int armor,dodge;
  public Defense(int armor,int dodge){
    this.armor = armor;
    this.dodge = dodge;
  }
  public int getArmor(){
    return armor;
  }
  public int getDodge(){
    return dodge;
  }
  public void setArmor(int armor){
    this.armor = armor;
  }
   public void setDodge(int dodge){
    this.dodge = dodge;
  }

}
