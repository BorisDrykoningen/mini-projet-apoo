/**
 * Une direction de déplacement, cohérente avec Position. Cette classe est
 * immuable
 * @see Position
 * @author Boris DRYKONINGEN
*/
public class Direction {
    /**
     * Le déplacement le long de l'axe des abscisses
    */
    private int x;


    /**
     * Le déplacement le long de l'axe des ordonnées
    */
    private int y;


    /**
     * Construit un déplacement arbitraire
     * @param x le déplacement le long de l'axe des abscisses
     * @param y le déplacement le long de l'axe des ordonnées
    */
    public Direction(int x, int y) {
        this.x = x;
        this.y = y;
    }


    /**
     * Donne accès au déplacement le long de l'axe des abscisses
     * @return le déplacement le long de l'axe des abscisses
    */
    public int getX() {
        return x;
    }


    /**
     * Donne accès au déplacement le long de l'axe des ordonnées
     * @return le déplacement le long de l'axe des ordonnées
    */
    public int getY() {
        return y;
    }
}

