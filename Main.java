/**
 * La classe principale du programme
 * @author Boris DRYKONINGEN
*/
public class Main {
    /**
     * La fonction principale du programme
     * @param args les arguments passés au programme via la CLI
    */
    public static void main(String[] args) {
        int l, h;
        Ecran.afficherln("Veuillez entrer la largeur de la grille");
        l = Clavier.saisirInt();
        Ecran.afficherln("Veuillez entrer la hauteur de la grille");
        h = Clavier.saisirInt();

        if (l < 2 || h < 2) {
            Ecran.afficherln("Grille trop petite !");
            return;
        }

        Partie p = new Partie(l, h, 2);
        p.ajouterJoueur(new Player(
            "Albert",
            'A',
            0,
            new Position(p.getGrilleGraphique(), 0, 0),
            new Attack(8, 4),
            new Defense(6, 2),
            new Health(10, 5)
        ));
        p.ajouterJoueur(new Player(
            "Bernard",
            'B',
            1,
            new Position(p.getGrilleGraphique(), l - 1, h - 1),
            new Attack(2, 10),
            new Defense(1, 7),
            new Health(11, 4)
        ));
        p.ajouterJoueur(new Player(
            "Camille",
            'C',
            0,
            new Position(p.getGrilleGraphique(), 1, 0),
            new Attack(4, 4),
            new Defense(10, 2),
            new Health(11, 4)
        ));
        p.ajouterJoueur(new Player(
            "Daisy",
            'D',
            1,
            new Position(p.getGrilleGraphique(), l - 2, h - 1),
            new Attack(2, 6),
            new Defense(4, 8),
            new Health(10, 5)
        ));
        p.jouer();
    }
}

